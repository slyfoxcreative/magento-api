<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento;

use SlyFoxCreative\Magento\Exceptions\NonexistentAttributeException;

class Product
{
    private $productId;
    private $session;
    private $storeViewId;
    private $data;
    private $attributes;

    public function __construct(Session $session, string $productId, array $attributes = [])
    {
        $this->session = $session;
        $this->productId = $productId;
        $this->storeViewId = $this->session->storeViewId;
        $this->data = $this->session->catalogProductInfo(
            $productId,
            $this->storeViewId,
            ['additional_attributes' => $attributes]
        );

        // Put additional attributes on the same level as regular attributes
        if (isset($this->data->additional_attributes)) {
            foreach ($this->data->additional_attributes as $attr) {
                $key = $attr->key;
                $this->data->{$key} = $attr->value;
            }
        }

        // Construct an array of attribute code => attribute info
        // This is necessary because some attributes (e.g., 'name') can't be
        // fetched by code for some reason, so we need to fetch them via id
        // instead.
        $this->attributes = array_reduce(
            $this->session->catalogProductAttributeList($this->data->set),
            function ($a, $i) {
                $a[$i->code] = $i;

                return $a;
            },
            []
        );
    }

    public function __get(string $name)
    {
        if (!isset($this->data->{$name})) {
            throw new NonexistentAttributeException($name, $this);
        }

        if (is_null($this->data->{$name})) {
            return;
        }

        // It's not a Magento attribute
        if (!in_array($name, array_keys($this->attributes))) {
            return $this->data->{$name};
        }

        // Convert attribute value based on type
        switch ($this->attributes[$name]->type) {
        case 'boolean':
            return $this->data->{$name} === '1';
        case 'date':
            return new \DateTime($this->data->{$name});
        case 'price':
            return round(floatval($this->data->{$name}), 2);
        case 'multiselect':
        case 'select':
            // Return the attribute label instead of its value
            return $this->session
                ->attribute((string) $this->attributes[$name]->attribute_id)
                ->options[$this->data->{$name}];
        default:
            return $this->data->{$name};
        }
    }

    public function disable(): bool
    {
        return $this->session->catalogProductUpdate(
            $this->productId,
            ['status' => 2],
            $this->storeViewId
        );
    }

    public function update(array $attributes): bool
    {
        return $this->session->catalogProductUpdate(
            $this->productId,
            $attributes,
            $this->storeViewId
        );
    }

    public static function products(Session $session, array $attributes = [], callable $filter = null): array
    {
        $ids = array_map(
            function ($p) {
                return $p->product_id;
            },
            $session->catalogProductList(null, $session->storeViewId)
        );

        $products = [];
        foreach ($ids as $id) {
            $product = new self($session, $id, $attributes);
            if (!is_null($filter) && !$filter($product)) {
                continue;
            }
            $products[] = $product;
        }

        return $products;
    }
}
