<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento;

class Category
{
    private $session;
    private $id;
    private $data;

    public function __construct(Session $session, string $id)
    {
        $this->session = $session;
        $this->id = $id;
        $this->data = $session->catalogCategoryInfo($id, $session->storeViewId);
    }

    public function __get(string $name)
    {
        return $this->data->{$name};
    }

    public function products(): array
    {
        return $this->session->catalogCategoryAssignedProducts($this->id);
    }

    public function assignProduct(string $productId): bool
    {
        return $this->session->catalogCategoryAssignProduct(
            $this->id,
            $productId
        );
    }

    public function removeProduct(string $productId): bool
    {
        return $this->session->catalogCategoryRemoveProduct(
            $this->id,
            $productId
        );
    }
}
