<?php

return [
    'sessions' => [
        'default' => [
            'url' => env('MAGENTO_URL'),
            'user' => env('MAGENTO_USER'),
            'key' => env('MAGENTO_KEY'),
            'site_id' => env('MAGENTO_SITE_ID'),
        ],
    ],
];
