<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento;

class Customer
{
    private $data;

    public function __construct(Session $session, string $customerId)
    {
        $this->data = $session->customerCustomerInfo($customerId);
    }

    public function __get(string $name)
    {
        switch ($name) {
        case 'name':
            return "{$this->data->firstname} {$this->data->lastname}";
        default:
            return $this->data->{$name};
        }
    }
}
