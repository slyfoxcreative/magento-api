<?php

namespace SlyFoxCreative\Magento\Facades;

use Illuminate\Support\Facades\Facade;

class Magento extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'slyfoxcreative.magento.default';
    }
}
