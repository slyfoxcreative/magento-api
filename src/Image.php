<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento;

class Image
{
    private $data;

    public function __construct(Session $session, string $productId, string $file)
    {
        $this->data = $session->catalogProductAttributeMediaInfo(
            $productId,
            $file,
            $session->storeViewId
        );
    }

    public function __get(string $name)
    {
        return $this->data->{$name};
    }

    public static function images(Session $session, string $productId): array
    {
        return array_map(
            function ($i) use ($session, $productId) {
                return new self($session, $productId, $i->file);
            },
            $session->catalogProductAttributeMediaList($productId, $session->storeViewId)
        );
    }
}
