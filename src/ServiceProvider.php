<?php

namespace SlyFoxCreative\Magento;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    private const CONFIG_FILE = __DIR__ . '/config/magento.php';

    public function boot()
    {
        $this->publishes([
            self::CONFIG_FILE => config_path(basename(self::CONFIG_FILE)),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            self::CONFIG_FILE,
            basename(self::CONFIG_FILE, '.php')
        );

        foreach ((array) config('magento.sessions') as $name => $c) {
            $this->app->singleton(
                "slyfoxcreative.magento.{$name}",
                function ($app) use ($c) {
                    $client = new \SoapClient($c['url']);
                    $session = $client->login($c['user'], $c['key']);

                    return new Session($client, $session, $c['site_id']);
                }
            );
        }
    }
}
