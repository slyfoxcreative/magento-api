<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento;

class Session
{
    public $storeViewId;
    private $client;
    private $session;
    private $attributes;

    public function __construct(\SoapClient $client, string $session, string $storeViewId = null)
    {
        $this->client = $client;
        $this->session = $session;
        $this->storeViewId = $storeViewId;
    }

    public function __call(string $method, array $arguments)
    {
        array_unshift($arguments, $this->session);

        return call_user_func_array([$this->client, $method], $arguments);
    }

    public function products(array $attributes = [], callable $filter = null): array
    {
        return Product::products($this, $attributes, $filter);
    }

    public function product(string $id, array $attributes = []): Product
    {
        return new Product($this, $id, $attributes);
    }

    public function images(string $productId): array
    {
        return Image::images($this, $productId);
    }

    public function image(string $productId, string $file): Image
    {
        return new Image($this, $productId, $file);
    }

    public function attribute(string $id): Attribute
    {
        if (!isset($this->attributes[$id])) {
            $this->attributes[$id] = new Attribute($this, $id);
        }

        return $this->attributes[$id];
    }

    public function category(string $id): Category
    {
        return new Category($this, $id);
    }

    public function customer(string $id): Customer
    {
        return new Customer($this, $id);
    }

    public function customerAddresses(string $customerId): array
    {
        return CustomerAddress::addresses($this, $customerId);
    }

    public function orders(callable $filter = null): array
    {
        return Order::orders($this, $filter);
    }

    public function order(string $orderId): Order
    {
        return new Order($this, $orderId);
    }
}
