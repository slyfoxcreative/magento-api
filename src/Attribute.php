<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento;

class Attribute
{
    private $data;

    public function __construct(Session $session, string $id)
    {
        $this->data = $session->catalogProductAttributeInfo($id);

        // Convert options array to a more useful form
        if (isset($this->data->options)) {
            $this->data->options = array_reduce(
                $this->data->options,
                function ($a, $i) {
                    $a[$i->value] = $i->label;

                    return $a;
                },
                []
            );
        }
    }

    public function __get(string $name)
    {
        return $this->data->{$name};
    }
}
