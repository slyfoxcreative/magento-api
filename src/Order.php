<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento;

class Order
{
    private $orderId;
    private $session;
    private $data;

    public function __construct(Session $session, string $orderId)
    {
        $this->session = $session;
        $this->orderId = $orderId;
        $this->data = $this->session->salesOrderInfo($this->orderId);
    }

    public function __get(string $name)
    {
        return $this->data->{$name};
    }

    public function products(): array
    {
        return array_map(
            function ($i) {
                return $this->session->product($i->product_id);
            },
            $this->items
        );
    }

    public static function orders(Session $session, callable $filter = null): array
    {
        return array_map(
            function ($o) use ($session) {
                return $session->order($o->increment_id);
            },
            array_filter(
                $session->salesOrderList(),
                function ($o) use ($filter) {
                    return $filter($o);
                }
            )
        );
    }
}
