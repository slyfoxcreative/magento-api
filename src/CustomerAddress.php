<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento;

class CustomerAddress
{
    private $data;

    public function __construct(\stdClass $data)
    {
        $this->data = $data;
    }

    public function __get(string $name)
    {
        return $this->data->{$name};
    }

    public static function addresses(Session $session, string $customerId)
    {
        return array_map(
            function ($a) {
                return new self($a);
            },
            $session->customerAddressList($customerId)
        );
    }
}
