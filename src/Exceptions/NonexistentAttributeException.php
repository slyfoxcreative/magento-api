<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento\Exceptions;

use DomainException;
use SlyFoxCreative\Magento\Product;
use Throwable;

class NonexistentAttributeException extends DomainException
{
    public function __construct(
        string $name,
        Product $product,
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct(
            "Attribute {$name} does not exist on product {$product->product_id}",
            $code,
            $previous
        );
    }
}
