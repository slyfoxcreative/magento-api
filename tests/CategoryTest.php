<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento\Tests;

class CategoryTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->client
            ->shouldReceive('catalogCategoryInfo')
            ->with('12345', '187', '2')
            ->andReturn($this->fixture('category_187'))
        ;

        $this->client
            ->shouldReceive('catalogCategoryAssignedProducts')
            ->with('12345', '187')
            ->andReturn($this->fixture('category_assigned_products_187'))
        ;

        $this->client
            ->shouldReceive('catalogCategoryAssignProduct')
            ->with('12345', '187', '3')
            ->andReturn(true)
        ;

        $this->client
            ->shouldReceive('catalogCategoryRemoveProduct')
            ->with('12345', '187', '3')
            ->andReturn(true)
        ;
    }

    public function testCategory()
    {
        $category = $this->session->category('187');

        $this->assertEquals('Chocks', $category->name);
    }

    public function testCategoryProducts()
    {
        $category = $this->session->category('187');

        $this->assertEquals(
            [2057, 47, 46, 45, 43],
            array_map(function ($p) {
                return $p->product_id;
            }, $category->products())
        );
    }

    public function testAssignProduct()
    {
        $category = $this->session->category('187');

        $this->assertTrue($category->assignProduct('3'));
    }

    public function testRemoveProduct()
    {
        $category = $this->session->category('187');

        $this->assertTrue($category->removeProduct('3'));
    }
}
