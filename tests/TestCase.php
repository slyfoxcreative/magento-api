<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento\Tests;

use PHPUnit\Framework\TestCase as BaseTestCase;
use SlyFoxCreative\Magento\Session;

class TestCase extends BaseTestCase
{
    protected function setUp(): void
    {
        $this->client = \Mockery::mock('SoapClient', [$_ENV['MAGENTO_URL']]);
        $this->session = new Session($this->client, '12345', '2');
    }

    protected function fixture(string $file)
    {
        return unserialize(file_get_contents(__DIR__ . "/fixtures/{$file}.txt"));
    }
}
