<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento\Tests;

class AttributeTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $attributes = [
            '175' => 'alternate_sku',
            '217' => 'absorbent_types',
        ];

        foreach ($attributes as $id => $code) {
            $this->client
                ->shouldReceive('catalogProductAttributeInfo')
                ->with('12345', $code)
                ->andReturn($this->fixture("attribute_{$id}"))
            ;
        }
    }

    public function testAttribute()
    {
        $attribute = $this->session->attribute('alternate_sku');

        $this->assertEquals(
            'Alternate SKU',
            $attribute->frontend_label[0]->label
        );
    }

    public function testAttributeOptions()
    {
        $attribute = $this->session->attribute('absorbent_types');

        $expected = [
            585 => 'Booms',
            795 => 'Loose Fill',
            583 => 'Pads',
            582 => 'Pillows',
            580 => 'Rags',
            584 => 'Rolls',
            581 => 'Socks',
            597 => 'Wiper',
        ];

        $this->assertEquals($expected, $attribute->options);
    }
}
