<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento\Tests;

use SlyFoxCreative\Magento\Exceptions\NonexistentAttributeException;

class ProductTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        foreach (['43', '45', '46', '47', '2057'] as $id) {
            $this->client
                ->shouldReceive('catalogProductInfo')
                ->with('12345', $id, '2', [
                    'additional_attributes' => [
                        'alternate_sku',
                        'promotional_shipping',
                    ],
                ])
                ->andReturn($this->fixture("product_{$id}"))
            ;
        }

        $this->client
            ->shouldReceive('catalogProductAttributeList')
            ->with('12345', '12')
            ->andReturn($this->fixture('attribute_set_12'))
        ;

        $this->client
            ->shouldReceive('catalogProductList')
            ->with('12345', null, '2')
            ->andReturn($this->fixture('product_list'))
        ;

        $this->client
            ->shouldReceive('catalogProductAttributeInfo')
            ->with('12345', '96')
            ->andReturn($this->fixture('attribute_96'))
        ;

        $this->client
            ->shouldReceive('catalogProductUpdate')
            ->with('12345', '43', ['status' => 2], '2')
            ->andReturn(true)
        ;

        $this->product = $this->session->product('43', [
            'alternate_sku',
            'promotional_shipping',
        ]);
    }

    public function testProduct()
    {
        $this->assertEquals('TTN*WC-1500X-OB', $this->product->alternate_sku);
    }

    public function testBooleanAttribute()
    {
        $this->assertFalse($this->product->promotional_shipping);
    }

    public function testDateAttribute()
    {
        $this->assertInstanceOf(\DateTime::class, $this->product->special_from_date);
    }

    public function testPriceAttribute()
    {
        $this->assertEquals(239.0, $this->product->price);
    }

    public function testSelectAttribute()
    {
        $this->assertEquals('Disabled', $this->product->status);
    }

    public function testNonexistentAttribute()
    {
        $this->expectException(NonexistentAttributeException::class);
        $this->product->fake_attribute;
    }

    public function testDisable()
    {
        $this->assertTrue($this->product->disable());
    }

    public function testUpdate()
    {
        $this->assertTrue($this->product->update(['status' => 2]));
    }

    public function testProducts()
    {
        $filter = function ($p) {
            return $p->type !== 'grouped';
        };
        $products = $this->session->products(
            ['alternate_sku', 'promotional_shipping'],
            $filter
        );

        foreach ($products as $product) {
            $this->assertTrue($filter($product));
        }
    }
}
