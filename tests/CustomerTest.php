<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento\Tests;

class CustomerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->client
            ->shouldReceive('customerCustomerInfo')
            ->with('12345', '2446')
            ->andReturn($this->fixture('customer_2446'))
        ;

        $this->client
            ->shouldReceive('customerAddressList')
            ->with('12345', '2446')
            ->andReturn($this->fixture('customer_address_list_2446'))
        ;
    }

    public function testCustomerName()
    {
        $customer = $this->session->customer('2446');

        $this->assertEquals('Steven Berg', $customer->name);
    }

    public function testCustomerAddresses()
    {
        $addresses = $this->session->customerAddresses('2446');

        $expected = [
            "4118 Creek Way\nApt B",
            '409 E 14 STREET',
        ];

        $this->assertEquals(
            $expected,
            array_map(function ($a) {
                return $a->street;
            }, $addresses)
        );
    }
}
