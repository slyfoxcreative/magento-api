<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento\Tests;

class ImageTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $images = [
            1 => '/w/c/wc-1500x-cp_cut_smallweb.png',
            2 => '/w/c/wc-1500x-cp_cut_og_smallweb.png',
            3 => '/c/u/customprofile_deepredharley_0015_cut-web_1.png',
            4 => '/c/u/customprofile_deepredharley_0023_cut-web_1.png',
            5 => '/c/p/cp_chock_indian_2.png',
        ];

        foreach ($images as $i => $file) {
            $this->client
                ->shouldReceive('catalogProductAttributeMediaInfo')
                ->with('12345', '2057', $file, '2')
                ->andReturn($this->fixture("product_image_2057_{$i}"))
            ;
        }

        $this->client
            ->shouldReceive('catalogProductAttributeMediaList')
            ->with('12345', '2057', '2')
            ->andReturn($this->fixture('product_image_list_2057'))
        ;
    }

    public function testImage()
    {
        $image = $this->session->image('2057', '/w/c/wc-1500x-cp_cut_smallweb.png');

        $this->assertEquals(
            'https://titanlifts.com/media/catalog/product/w/c/wc-1500x-cp_cut_smallweb.png',
            $image->url
        );
    }

    public function testImages()
    {
        $images = $this->session->images('2057');

        $expected = [
            'https://titanlifts.com/media/catalog/product/w/c/wc-1500x-cp_cut_smallweb.png',
            'https://titanlifts.com/media/catalog/product/w/c/wc-1500x-cp_cut_og_smallweb.png',
            'https://titanlifts.com/media/catalog/product/c/u/customprofile_deepredharley_0015_cut-web_1.png',
            'https://titanlifts.com/media/catalog/product/c/u/customprofile_deepredharley_0023_cut-web_1.png',
            'https://titanlifts.com/media/catalog/product/c/p/cp_chock_indian_2.png',
        ];

        $this->assertEquals(
            $expected,
            array_map(function ($i) {
                return $i->url;
            }, $images)
        );
    }
}
