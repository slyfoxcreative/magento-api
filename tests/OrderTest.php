<?php

declare(strict_types=1);

namespace SlyFoxCreative\Magento\Tests;

class OrderTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->client
            ->shouldReceive('salesOrderInfo')
            ->with('12345', '100005663')
            ->andReturn($this->fixture('order_100005663'))
        ;

        foreach (['22', '24'] as $id) {
            $this->client
                ->shouldReceive('catalogProductInfo')
                ->with('12345', $id, '2', ['additional_attributes' => []])
                ->andReturn($this->fixture("product_{$id}"))
            ;
        }

        $this->client
            ->shouldReceive('catalogProductAttributeList')
            ->with('12345', '9')
            ->andReturn($this->fixture('attribute_set_9'))
        ;

        $this->client
            ->shouldReceive('salesOrderList')
            ->with('12345')
            ->andReturn($this->fixture('order_list'))
        ;

        $this->order = $this->session->order('100005663');
    }

    public function testOrder()
    {
        $this->assertEquals('100005663', $this->order->increment_id);
    }

    public function testProducts()
    {
        $this->assertEquals(
            [22, 24],
            array_map(function ($p) {
                return $p->product_id;
            }, $this->order->products())
        );
    }

    public function testOrders()
    {
        $filter = function ($o) {
            return in_array($o->status, ['pending', 'processing'])
                && $o->shipping_method === 'excelfreight_terminal_delivery';
        };

        $orders = $this->session->orders($filter);

        foreach ($orders as $order) {
            $this->assertTrue($filter($order));
        }
    }
}
