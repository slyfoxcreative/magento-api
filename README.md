Library for accessing the Magento SOAP API

``` php
use \SlyFoxCreative\Magento\Session;

$client = new \SoapClient($url);
$sessionId = $client->login($username, $password);
$session = new Session($client, $sessionId, $siteId);
$customer = $session->customer('4318');

echo "$customer->name\n";

$product = $session->product('42', ['color', 'shipping_individual']);
echo "$product->color\n";
if ($product->shipping_individual) {
    echo "$product->name ships individually\n";
}
```

Versioning should be according to [Semantic Versioning](http://semver.org).

To use in your project, include in your composer.json:

``` json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://composer.slyfoxcreative.com"
        }
    ],
    "require": {
        "slyfoxcreative/magento": "^7.0" # use the current version
    }
}
```

To add to a Laravel app:

Add the service provider and facade to `config/app.php`:

``` php
'providers' => [
    ...
    SlyFoxCreative\Magento\MagentoServiceProvider::class,
],
...
'aliases' => [
    ...
    'Magento' => SlyFoxCreative\Magento\Facades\Magento::class,
],
```

Publish the configuration file to `config/magento.php`:

``` bash
$ php artisan vendor:publish --provider="SlyFoxCreative\Magento\MagentoServiceProvider"
```

Use via the facade:

``` php
$customer = Magento::customer('4318');
```
